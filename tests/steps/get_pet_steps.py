from behave import given, when, then
from pprint import pprint

import config
from page_objects.pet_page import get_pet, get_pet_by_status

debug = config.debug


@when('I get pets by status "{status}"')
def step_impl(context, status):
    response = get_pet_by_status(status)
    assert response.status_code == 200
    context.pet = response.json()


@then('Pets with status "{status}" should return with success')
def step_impl(context, status):
    if debug:
        print(len(context.pet))
    for pet in context.pet:
        assert pet['status'] == status


@when('I get a pet by id "{pet_id}"')
def step_impl(context, pet_id):
    response = get_pet(pet_id)
    context.pet_response = response


@when("I get a pet by id")
def step_impl(context):
    response = get_pet(context.pet_id)
    context.pet_response = response

