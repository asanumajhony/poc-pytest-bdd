from behave import given, when, then

from page_objects.pet_page import delete_pet_by_id


@given('I have removed a pet by id "{pet_id}"')
@when('I remove a pet by id "{pet_id}"')
def step_impl(context, pet_id):
    response = delete_pet_by_id(pet_id)
    assert response.status_code == 200 or 404, f'{response.status_code} != 200 or 404'
    context.pet_response = response


@when("I remove a pet by id")
def step_impl(context):
    response = delete_pet_by_id(context.pet_id)
    assert response.status_code == 200, f'{response.status_code} != 200'
    context.pet_response = response
