from behave import given, when, then


@then("The pet will be deleted with success")
@then("A response with fail will return")
def step_impl(context):
    for row in context.table:
        if row['key'] == 'status_code':
            assert context.pet_response.status_code == int(row['value']), f"{context.pet_response.status_code} != {int(row['value'])}"
        else:
            resp = context.pet_response.json()
            assert resp.get(row['key']) == row['value'], f"Expected {resp.get(row['key'])} to be equal {row['value']}"
