from behave import given, when, then
from re import search

from page_objects.pet_page import get_random_dog_image, upload_image


@when('I try to upload a image of a pet with')
@when('I upload a image of this pet with')
def step_impl(context):
    random_image = get_random_dog_image()
    pet_id = None
    additional_metadata = None
    for row in context.table:
        if row['key'] == 'id':
            pet_id = row['value']
        elif row['key'] == 'additional_metadata':
            additional_metadata = row['value']
        else:
            raise Exception(f'Key {row["key"]} not implemented. Check your code.')
    response = upload_image(pet_id, random_image.content, additional_metadata)
    assert response.status_code == 200 or 404
    context.pet_response = response


@then('A response from image upload will return')
def step_impl(context):
    for row in context.table:
        if row['key'] == 'status_code':
            assert context.pet_response.status_code == int(row['value']), f"{context.pet_response.status_code} != {int(row['value'])}"
        else:
            resp = context.pet_response.json()
            print(resp.get(row['key']), ' ==== ', row['value'])
            assert search(row['value'], resp.get(row['key'])), f"Expected {resp.get(row['key'])} to includes {row['value']}"
