from behave import given, when, then
import json

from page_objects.pet_page import create_pet
from dto.pet_bean import Pet


@given('I have a pet with')
@when('I create a pet with')
@when("I try to create a pet with")
def step_impl(context):
    context.pet = Pet()
    for row in context.table:
        if row['key'] == 'id':
            if row['value'] != 'empty':
                setattr(context.pet, row['key'], row['value'])
        elif row['key'] == 'category':
            setattr(context.pet, row['key'], json.loads(row['value']))
        elif row['key'] == 'tags':
            context.pet.add_tags(json.loads(row['value']))
        elif row['key'] == 'photo_urls':
            context.pet.add_photo_url(row['value'])
        else:
            setattr(context.pet, row['key'], row['value'])
    if hasattr(context, 'pet_tags'):
        setattr(context.pet, 'tags', context.pet_tags)
    response = create_pet(context)
    context.pet_response = response
    pet_response = response.json()
    context.pet_id = pet_response.get('id')


@given("I set tags for a pet")
def step_impl(context):
    tags = []
    for row in context.table:
        tag = {
            "id": row['tag_id'],
            "name": row['tag_name']
        }
        tags.append(tag)
    context.pet_tags = tags


@then("The updated pet should return")
@then("The created pet should return")
def step_impl(context):
    resp = context.pet_response.json()
    for row in context.table:
        if row['key'] == 'id':
            pet_id = resp.get('id')
            if row['value'] != 'true':
                assert pet_id == row['value']
            else:
                assert pet_id is not None
        elif row['key'] == 'status_code':
            assert context.pet_response.status_code == int(row['value'])
        elif row['key'] == 'category':
            assert resp.get(row['key']) == json.loads(row['value']), f"Expected {resp.get(row['key'])} to be equal {row['value']}"
        else:
            assert resp.get(row['key']) == row['value'], f"Expected {resp.get(row['key'])} to be equal {row['value']}"


@then("The pet should contain tags")
def step_impl(context):
    resp = context.pet_response.json()
    resp_tags = resp.get('tags')
    for row in context.table:
        found = next((item for item in resp_tags if item['id'] == int(row['tag_id'])), None)
        assert found is not None
        assert found['id'] == int(row['tag_id'])
        assert found['name'] == row['tag_name']
