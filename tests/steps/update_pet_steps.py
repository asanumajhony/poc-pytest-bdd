from behave import given, when, then
import json
from pprint import pprint

from page_objects.pet_page import update_pet, update_pet_by_form_data
from dto.pet_bean import Pet


@when("I update this pet with")
def step_impl(context):
    context.pet = Pet()
    for row in context.table:
        if row['key'] == 'id':
            if row['value'] != 'true':
                setattr(context.pet, row['key'], row['value'])
            else:
                setattr(context.pet, row['key'], context.pet_id)
        elif row['key'] == 'category':
            setattr(context.pet, row['key'], json.loads(row['value']))
        elif row['key'] == 'tags':
            context.pet.add_tags(json.loads(row['value']))
        elif row['key'] == 'photo_urls':
            context.pet.add_photo_url(row['value'])
        else:
            setattr(context.pet, row['key'], row['value'])
    if hasattr(context, 'pet_tags'):
        setattr(context.pet, 'tags', context.pet_tags)
    response = update_pet(context)
    context.pet_response = response


@when("I update this pet with by form data with")
def step_impl(context):
    context.pet = {}
    pet_id = None
    for row in context.table:
        if row['key'] == 'id':
            pet_id = row['value']
        else:
            context.pet[row['key']] = row['value']
    context.pet_response = update_pet_by_form_data(context, pet_id)


@then("The pet will be updated with success")
def step_impl(context):
    resp = context.pet_response.json()
    for row in context.table:
        if row['key'] == 'status_code':
            assert context.pet_response.status_code == int(row['value'])
        elif row['key'] == 'message':
            assert resp.get(row['key']) == row['value'], f"Expected {resp.get(row['key'])} to be equal {row['value']}"
        else:
            raise Exception(f'Key {row["key"]} not implemented. Check your code.')
