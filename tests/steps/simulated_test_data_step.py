from behave import given, when, then

from page_objects.simulated_test_data_page import get_random_coordinates, calculate_route


@given('I have the random coordinates from')
def step_impl(context):
    country_code = None
    for row in context.table:
        country_code = row['country_code']

    context.coordinates = get_random_coordinates(country_code)


@when('I simulate the driving distance')
def step_impl(context):
    response = calculate_route(context)
    assert response.status_code == 200, f'Response status_code: {response.status_code}\n{response.text}'
    simulation = response.json()
    context.distance = simulation['routes'][0]['summary']['lengthInMeters']
    print(context.distance)


@then('The simulation should return a route distance')
def step_impl(context):
    assert context.distance >= 0, f'Distance: {context.distance} should be not zero.'
