@simulation
Feature: Simulated test data

	Scenario: Simulated travel distance
		Given I have the random coordinates from
			| country_code |
			| US           |
		When I simulate the driving distance
		Then The simulation should return a route distance
