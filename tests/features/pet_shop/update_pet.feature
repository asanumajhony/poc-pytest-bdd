@update_pet
Feature: Pet api - Update Pet
	Using https://petstore.swagger.io/#/
	Create a sample project using Postman for the ‘pet’ part of the sample api.

	Scenario: Update a pet by id with success
		Given I have a pet with
			| key | value |
			| id  | empty |
		When I update this pet with
			| key      | value                       |
			| id       | true                        |
			| category | {"id": 3,"name": "hamster"} |
			| name     | brown                       |
			| status   | pending                     |
		Then The updated pet should return
			| key         | value                       |
			| status_code | 200                         |
			| id          | true                        |
			| category    | {"id": 3,"name": "hamster"} |
			| name        | brown                       |
			| status      | pending                     |

	Scenario: Create a pet by PUT method with success
		Given I have removed a pet by id "123456789"
		When I update this pet with
			| key      | value                       |
			| id       | 123456789                   |
			| category | {"id": 3,"name": "hamster"} |
			| name     | brown                       |
			| status   | pending                     |
		Then The created pet should return
			| key         | value                       |
			| status_code | 200                         |
			| id          | true                        |
			| category    | {"id": 3,"name": "hamster"} |
			| name        | brown                       |
			| status      | pending                     |

	Scenario: Fail to update a pet by invalid id
		Given I have a pet with
			| key | value |
			| id  | empty |
		When I update this pet with
			| key      | value                       |
			| id       | invalid                     |
			| category | {"id": 3,"name": "hamster"} |
			| name     | brown                       |
			| status   | pending                     |
		Then A response with fail will return
			| key         | value                  |
			| status_code | 500                    |
			| message     | something bad happened |

	Scenario: Update a pet by form data with success
		Given I have a pet with
			| key | value     |
			| id  | 123456789 |
		When I update this pet with by form data with
			| key    | value     |
			| id     | 123456789 |
			| name   | gold      |
			| status | available |
		Then The pet will be updated with success
			| key         | value     |
			| status_code | 200       |
			| message     | 123456789 |

	Scenario: Fail to update a pet by form data with invalid id
		When I update this pet with by form data with
			| key    | value     |
			| id     | invalid   |
			| name   | gold      |
			| status | available |
		Then A response with fail will return
			| key         | value                                                        |
			| status_code | 404                                                          |
			| message     | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Fail to update a pet by form data with non existent id
		Given I have removed a pet by id "123456789"
		When I update this pet with by form data with
			| key    | value     |
			| id     | 123456789 |
			| name   | gold      |
			| status | available |
		Then A response with fail will return
			| key         | value     |
			| status_code | 404       |
			| message     | not found |

