@get_pet
Feature: Pet api - Get Pet
	Using https://petstore.swagger.io/#/
	Create a sample project using Postman for the ‘pet’ part of the sample api.

	Scenario Outline: Get pets by Status scenarios
		When I get pets by status "<status>"
		Then Pets with status "<status>" should return with success
		Examples:
			| status    |
			| available |
			| pending   |
			| sold      |

	Scenario: Get a pet by id with success
		Given I have a pet with
			| key      | value                   |
			| id       | 123456789               |
			| category | {"id": 2,"name": "dog"} |
			| name     | Max                     |
			| status   | sold                    |
		When I get a pet by id
		Then The created pet should return
			| key         | value                   |
			| status_code | 200                     |
			| id          | true                    |
			| category    | {"id": 2,"name": "dog"} |
			| name        | Max                     |
			| status      | sold                    |

	Scenario: Fail to get a pet by invalid Id
		Given I have a pet with
			| key      | value                   |
			| id       | 123456789               |
			| category | {"id": 2,"name": "dog"} |
			| name     | Max                     |
			| status   | sold                    |
		When I get a pet by id "invalid"
		Then A response with fail will return
			| key         | value                                                        |
			| status_code | 404                                                          |
			| message     | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Fail to get a pet by non existent Id
		Given I have removed a pet by id "123456789"
		When I get a pet by id "123456789"
		Then A response with fail will return
			| key         | value         |
			| status_code | 404           |
			| message     | Pet not found |

