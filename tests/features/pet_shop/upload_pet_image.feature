@pet_image_upload
Feature: Pet api - Upload image
	Using https://petstore.swagger.io/#/
	Create a sample project using Postman for the ‘pet’ part of the sample api.

	Scenario: Upload a pet image with success
		Given I have a pet with
			| key | value     |
			| id  | 123456789 |
		When I upload a image of this pet with
			| key                 | value       |
			| id                  | 123456789   |
			| additional_metadata | Pet picture |
		Then A response from image upload will return
			| key         | value                           |
			| status_code | 200                             |
			| message     | additionalMetadata: Pet picture |

	Scenario: Fail to upload a pet image with an invalid id
		When I try to upload a image of a pet with
			| key                 | value       |
			| id                  | invalid     |
			| additional_metadata | Pet picture |
		Then A response from image upload will return
			| key         | value                                                        |
			| status_code | 404                                                          |
			| message     | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Upload a pet image with an non existent id*
		Given I have removed a pet by id "123456789"
		When I try to upload a image of a pet with
			| key                 | value       |
			| id                  | 123456789   |
			| additional_metadata | Pet picture |
		Then A response from image upload will return
			| key         | value                           |
			| status_code | 200                             |
			| message     | additionalMetadata: Pet picture |
