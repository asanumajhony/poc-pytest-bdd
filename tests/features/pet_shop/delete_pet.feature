@delete_pet
Feature: Pet api - Delete Pet
	Using https://petstore.swagger.io/#/
	Create a sample project using Postman for the ‘pet’ part of the sample api.

	Scenario: Delete a pet by id with success
		Given I have a pet with
			| key | value     |
			| id  | 123456789 |
		When I remove a pet by id
		Then The pet will be deleted with success
			| key         | value     |
			| status_code | 200       |
			| message     | 123456789 |

	Scenario: Fail to delete a pet with an invalid id
		When I remove a pet by id "invalid"
		Then A response with fail will return
			| key         | value                                                        |
			| status_code | 404                                                          |
			| message     | java.lang.NumberFormatException: For input string: "invalid" |

	Scenario: Fail to delete a pet with an non existent id
		Given I have removed a pet by id "123456789"
		When I remove a pet by id "123456789"
		Then A response with fail will return
			| key         | value |
			| status_code | 404   |
