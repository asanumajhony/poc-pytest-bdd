@create_pet
Feature: Pet api - Create Pet
	Using https://petstore.swagger.io/#/
	Create a sample project using Postman for the ‘pet’ part of the sample api.

	Scenario: Create a pet with success
		Given I set tags for a pet
			| tag_id | tag_name |
			| 10     | male     |
			| 11     | white    |
		When I create a pet with
			| key      | value                   |
			| id       | empty                   |
			| category | {"id": 1,"name": "cat"} |
			| name     | Tiger                   |
			| status   | available               |
		Then The created pet should return
			| key         | value                   |
			| status_code | 200                     |
			| id          | true                    |
			| category    | {"id": 1,"name": "cat"} |
			| name        | Tiger                   |
			| status      | available               |
		And The pet should contain tags
			| tag_id | tag_name |
			| 10     | male     |
			| 11     | white    |

	Scenario: Fail to create a pet with invalid ID
		When I try to create a pet with
			| key      | value                   |
			| id       | invalid                 |
			| category | {"id": 1,"name": "cat"} |
			| name     | Tiger                   |
			| status   | available               |
		Then A response with fail will return
			| key         | value                  |
			| status_code | 500                    |
			| message     | something bad happened |

	Scenario: Fail to create a pet with invalid category ID
		When I try to create a pet with
			| key      | value                           |
			| id       | 123                             |
			| category | {"id": "invalid","name": "cat"} |
			| name     | Tiger                           |
			| status   | available                       |
		Then A response with fail will return
			| key         | value                  |
			| status_code | 500                    |
			| message     | something bad happened |

	Scenario: Fail to create a pet with invalid tag ID
		Given I set tags for a pet
			| tag_id | tag_name |
			| invalid | male    |
		When I try to create a pet with
			| key      | value                   |
			| id       | 123                     |
			| category | {"id": 1,"name": "cat"} |
			| name     | Tiger                   |
			| status   | available               |
		Then A response with fail will return
			| key         | value                  |
			| status_code | 500                    |
			| message     | something bad happened |
