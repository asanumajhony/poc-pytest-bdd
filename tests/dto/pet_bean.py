class Pet:
    def __init__(self, id=None, category=None, name='Max', photoUrls=None, tags=None, status='available'):
        if tags is None:
            tags = []
        if photoUrls is None:
            photoUrls = []
        if category is None:
            category = {
                'id': 10,
                'name': 'dog'
            }
        self.id = id
        self.category = category
        self.name = name
        self.photoUrls = photoUrls
        self.tags = tags
        self.status = status

    def add_tags(self, tag):
        self.tags.append(tag)

    def add_photo_url(self, photo_url):
        self.photoUrls.append(photo_url)
