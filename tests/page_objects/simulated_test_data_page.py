import requests
from pprint import pprint
from faker import Faker
import config


debug = config.debug
tomtom_url = config.tomtom_url
tomtom_key = config.tomtom_key


def get_random_coordinates(country_code):
    fake = Faker()
    coordinates = []
    for _ in range(2):
        geo_map = fake.local_latlng(country_code=country_code)
        coordinate = f'{geo_map[0]},{geo_map[1]}'
        coordinates.append(coordinate)
    return coordinates


def calculate_route(context):
    coordinate_1 = context.coordinates[0]
    coordinate_2 = context.coordinates[1]
    print(coordinate_1, coordinate_2)
    url = f'{tomtom_url}/{coordinate_1}:{coordinate_2}/json?key={tomtom_key}'
    r = requests.get(url=url)
    if debug:
        pprint(r.json())
    return r


