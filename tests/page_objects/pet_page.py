import requests
from pprint import pprint
import config

base_url = config.base_url
resource = 'pet'
debug = config.debug


def create_pet(context):
    url = f'{base_url}/{resource}'
    payload = context.pet.__dict__
    r = requests.post(url=url, json=payload)
    if debug:
        pprint(r.json())
    return r


def update_pet(context):
    url = f'{base_url}/{resource}'
    payload = context.pet.__dict__
    r = requests.put(url=url, json=payload)
    if debug:
        pprint(r.json())
    return r


def update_pet_by_form_data(context, pet_id):
    url = f'{base_url}/{resource}/{pet_id}'
    r = requests.post(url=url, data=context.pet)
    if debug:
        pprint(r.json())
    return r


def get_pet(pet_id):
    url = f'{base_url}/{resource}/{pet_id}'
    r = requests.get(url=url)
    if debug:
        pprint(r.json())
    return r


def get_pet_by_status(status):
    url = f'{base_url}/{resource}/findByStatus?status={status}'
    r = requests.get(url=url)
    if debug:
        pprint(r.json())
    return r


def get_random_dog_image():
    url = 'https://dog.ceo/api/breeds/image/random'
    r = requests.get(url=url)
    response = r.json()
    if debug:
        pprint(response)
    image_url = response.get('message')
    img_r = requests.get(url=image_url)
    return img_r


def upload_image(pet_id, raw_image, additional_metadata):
    url = f'{base_url}/{resource}/{pet_id}/uploadImage'
    files = {'file': ('image.jpg', raw_image)}
    form_data = {
        "additionalMetadata": additional_metadata
    }
    r = requests.post(url=url, files=files, data=form_data)
    if debug:
        pprint(r.json())
    return r


def delete_pet_by_id(pet_id):
    url = f'{base_url}/{resource}/{pet_id}'
    r = requests.delete(url=url)
    if debug:
        if r.status_code == 404:
            print('delete =>', r.status_code)
        else:
            pprint(r.json())
    return r
