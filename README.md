# POC - Python + ~~Pytest-BDD~~ Behave + Requests

This repository is a POC (Proof of Concept) using:

- Python
- [Behave](https://behave.readthedocs.io/en/stable/index.html) 
- [Requests](https://docs.python-requests.org/en/master/)

This is an implementation of previous *Task 3* Swagger Pet Store scenarios:
> Using https://petstore.swagger.io/#/ Create a sample project using Postman for the ‘pet’ part of the sample api.

### Why I had to move from Pytest-BDD to Behave:

In a nutt shell, I have started this POC with [Pytest-BDD](https://github.com/pytest-dev/pytest-bdd), but during the project setup I have found that it not supports a Data Table, so I switched to Behave, another Python BDD framework that support partially Cucumber Data Tables.

Supported Data table implementation with Behave:

    Given I set tags for a pet
    | tag_id | tag_name | another_field |
    | 10     | male     | value_1       |
    | 11     | white    | value_2       |

The Data table above is a list of rows, where first line is the header and each line of table is the value.

Behave not supports a Map<String, String> / rows_hash() method implementation of Cucumber Data table, implemented in other languages like [Java](https://github.com/cucumber/cucumber/tree/master/datatable#datatable), [JavaScript](https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/data_table_interface.md#data-tables) and [Ruby](https://www.rubydoc.info/github/cucumber/cucumber-ruby/Cucumber%2FMultilineArgument%2FDataTable:rows_hash).

    When I create a pet with
    | id           | empty     |
    | categoryId   | 1         |
    | categoryName | cat       |
    | name         | Tiger     |
    | status       | available |

For those cases, the solution was adding a `key` and `value` headers on Data Table and implement a condition by `row['key']`when the field assign was needed.

### Pre-requisites & Installation

- Python 3

Install dependencies:

    pip3 install -r requirements.txt

### Test execution

To run all test scenarios, you can just run Behave by following command:

    behave

Behave test execution:

![behave_run](readme/behave_run.png)

To run a specific tag, just add `-t` parameter before tags expression:

    behave -t @create_pet

![behave_run_tags](readme/behave_run_tags.png)

### Test Results

Test execution:

![test_results](readme/test_results.png)


### Using simulate test-data on tests

I've created a simulated test data feature that uses:

- [Faker library](https://faker.readthedocs.io/en/master/index.html) to generate a random coordinates
- Integrate to [tomtom API](https://developer.tomtom.com/content/routing-api-explorer) to calculate the route distance between those 2 points

#### How to run
Execute from terminal:

    behave -t @simulation

The test results will returned:

![behave_simulation](readme/behave_simulate.png)

If you configure your IDE test runner to execute this scenario, you can see the outputs of coordinates and distance:

![simulation_ide_runner](readme/simulation_ide_runner.png)

### Simulated test data on pet-shop scenarios

The pet-shop feature also include a simulated data at image upload scenario.

This scenario gets each time a new dog image from [dog-api](https://dog.ceo/dog-api/) and uploads it to pet-shop API.
